// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "InviteeEmojiWidget.generated.h"

UCLASS()
class VRBANISM_0043_API UInviteeEmojiWidget : public UUserWidget
{
	GENERATED_BODY()

public:  // Public functions
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetEmoji(const FString & Emoji);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void HideEmoji();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ExecuteAnimation();

};
